# t3-reaper-playground

A collection of odds and ends that I use the aid in recording and mixing music with [Reaper](http://reaper.fm/).

## control-web-pages/
Reaper supports a web remote control interface, which also allows for serving up custom pages. Here are some custom pages (one simple one so far).
 
Instructions: 
1. Enable remote web control: see http://reaperblog.net/2016/12/whats-new-in-reaper-v-5-30/
2. For a single file, such as recording-monitor.html, right click on the page link below and select "save as" and save to the "Plugins/reaper_www_root" folder. For all files, Copy all files in the control-web-pages directory to the "Plugins/reaper_www_root" folder. 
3. Access the page at  http://<the-url-you-created-in-step-1>/    
   Example: ```http://localhost:8082/recording-monitor.html```

Pages:
* [recording-monitor.html](https://gitlab.com/tom.wadzinski/t3-reaper-playground/raw/master/control-web-pages/recording-monitor.html) - Basically a studio recording light. This single purpose reaper web page shows a full screen indicator of the current recording status. I was inspired to create this after all too frequently forgetting to start recording during an extended jam session. Using this page makes it so anyone can quickly see that recording is not turned on. You might want to display it on a separate monitor or phone/tablet somewhere visible in the studio space.
  
Demo: (Note: The gradient looks better in real life)
![recording-monitor](images/recording-monitor.gif?raw=true)

